using System;
using System.Collections.Generic;
using System.Diagnostics.CodeAnalysis;
using UnityEditor;
using UnityEngine;

namespace SpaceCaves
{
    public class Chunk : MonoBehaviour
    {
        private MarchingCubeTables mct;
        private float[,,] chunkVals;
        private Vector3 chunkCorner;
        private List<Vector3> meshVertices;
        public Vector3Int chunkWorldCoords;

        [HideInInspector] public Vector3 chunkCenter;

        [Header("Mesh")] public Material material;
        private Mesh mesh;
        private MeshFilter meshFilter;
        private MeshRenderer meshRenderer;
        private MeshCollider meshCollider;

        [Header("Chunk Settings")] [Range(2, 20)]
        public int chunkSize;
        [HideInInspector]public int actualChunkSize;

        public int zoom;
        [Range(0, 1f)] public float frequency;
        public int seed;

        [Header("")] [Range(0, 1)] public float activeVal;
        public bool interpolate;

        [Header("Gizmos/Dev")] public bool drawChunkBorders;
        public bool drawSpheres;
        public bool drawLabels;
        public float sphereRadius = 1.0f;
        public Color activeCol = Color.white;
        public Color inactiveCol = Color.black;


        public void Initialize(Vector3Int _worldCoord, int _chunkSize, int _zoom, float _frequency, int _seed, float _activeVal,
            Material _material, bool _useInterpolation)
        {
            chunkWorldCoords = _worldCoord;
            gameObject.name = $"Chunk-{_worldCoord.x},{_worldCoord.y},{_worldCoord.z}";
            // ReSharper disable once Unity.InefficientPropertyAccess
            gameObject.tag = "Terrain";
            
            chunkSize = _chunkSize;
            actualChunkSize = _chunkSize + 1;
            
            zoom = _zoom;
            frequency = _frequency / 100;
            seed = _seed;
            activeVal = _activeVal;
            meshRenderer.material = material = _material;
            interpolate = _useInterpolation;

            chunkCorner = transform.position;
            chunkCenter = new Vector3(
                chunkCorner.x + chunkSize * zoom / 2f,
                chunkCorner.y + chunkSize * zoom / 2f,
                chunkCorner.z + chunkSize * zoom / 2f);
            chunkVals = new float[actualChunkSize, actualChunkSize, actualChunkSize];

            GetChunkValues();
            UpdateMesh();
        }

        private void InstantiateMesh()
        {
            mct = new MarchingCubeTables();
            meshVertices = new List<Vector3>();
            mesh = new Mesh();


            meshFilter = GetComponent<MeshFilter>();
            meshRenderer = GetComponent<MeshRenderer>();
            meshCollider = GetComponent<MeshCollider>();

            if (meshFilter == null)
            {
                meshFilter = gameObject.AddComponent<MeshFilter>();
            }

            if (meshRenderer == null)
            {
                meshRenderer = gameObject.AddComponent<MeshRenderer>();
            }

            if (meshCollider == null)
            {
                meshCollider = gameObject.AddComponent<MeshCollider>();
            }

            mesh = meshFilter.sharedMesh;
            meshRenderer.material = material;

            if (mesh != null) return;
            mesh = new Mesh
            {
                indexFormat = UnityEngine.Rendering.IndexFormat.UInt32,
            };
            meshFilter.sharedMesh = mesh;
            meshCollider.sharedMesh = mesh;

            meshCollider.enabled = false;
            // ReSharper disable once Unity.InefficientPropertyAccess
            meshCollider.enabled = true;
        }


        private void Awake()
        {
            InstantiateMesh();
        }

        private void FixedUpdate()
        {
            // for some reason, even if the colliders convex is unchecked in editor,
            // it needs to be set to true and back to false to work, this forces false every update
            meshCollider.convex = false;
        }

        public void UpdateMesh()
        {
            GenerateMesh();
            BuildMesh();
        }

        public void SetActiveStatus(bool _active)
        {
            meshRenderer.enabled = _active;
        }

        private void GetChunkValues()
        {
            for (int x = 0; x < actualChunkSize; x++)
            {
                for (int y = 0; y < actualChunkSize; y++)
                {
                    for (int z = 0; z < actualChunkSize; z++)
                    {
                        Vector3 pointPos = new(
                            (x + chunkCorner.x / zoom) + seed,
                            (y + chunkCorner.y / zoom) + seed,
                            (z + chunkCorner.z / zoom) + seed
                        );

                        chunkVals[x, y, z] = (NoiseGenerator.Get3DPerlinNoise(pointPos, frequency) + 1) / 2f;
                    }
                }
            }
        }

        private void GenerateMesh()
        {
            meshVertices.Clear();
            for (int x = 0; x < actualChunkSize; x++)
            {
                for (int y = 0; y < actualChunkSize; y++)
                {
                    for (int z = 0; z < actualChunkSize; z++)
                    {
                        if (x == chunkSize || y == chunkSize || z == chunkSize) break;
                        int cellIndex = 0;

                        if (chunkVals[x, y, z + 1] > activeVal) cellIndex |= 1; // 0
                        if (chunkVals[x + 1, y, z + 1] > activeVal) cellIndex |= 2; // 1
                        if (chunkVals[x + 1, y, z] > activeVal) cellIndex |= 4; // 2
                        if (chunkVals[x, y, z] > activeVal) cellIndex |= 8; // 3
                        if (chunkVals[x, y + 1, z + 1] > activeVal) cellIndex |= 16; // 4
                        if (chunkVals[x + 1, y + 1, z + 1] > activeVal) cellIndex |= 32; // 5
                        if (chunkVals[x + 1, y + 1, z] > activeVal) cellIndex |= 64; // 6
                        if (chunkVals[x, y + 1, z] > activeVal) cellIndex |= 128; // 7


                        int[,] triangulation = mct.triangulation;

                        for (int i = 0; triangulation[cellIndex, i] != -1; i += 3)
                        {
                            Vector3Int cellCorner = new(x * zoom, y * zoom, z * zoom);

                            if (!interpolate)
                            {
                                meshVertices.Add(GetTriangleWorldCoord(cellCorner, triangulation[cellIndex, i]));
                                meshVertices.Add(GetTriangleWorldCoord(cellCorner, triangulation[cellIndex, i + 1]));
                                meshVertices.Add(GetTriangleWorldCoord(cellCorner, triangulation[cellIndex, i + 2]));
                            }
                            else
                            {
                                try
                                {
                                    Vector4[] w1vectors = GetCellCornersCoords(cellCorner, triangulation[cellIndex, i]);
                                    Vector4[] w2vectors =
                                        GetCellCornersCoords(cellCorner, triangulation[cellIndex, i + 1]);
                                    Vector4[] w3vectors =
                                        GetCellCornersCoords(cellCorner, triangulation[cellIndex, i + 2]);

                                    Vector3 interVert1 = InterpolateEdgePosition(new[]
                                    {
                                        new Vector4(w1vectors[0].x, w1vectors[0].y, w1vectors[0].z,
                                            GetCellCornerValue(cellCorner / zoom, (int)w1vectors[0].w)),
                                        new Vector4(w1vectors[1].x, w1vectors[1].y, w1vectors[1].z,
                                            GetCellCornerValue(cellCorner / zoom, (int)w1vectors[1].w))
                                    });

                                    Vector3 interVert2 = InterpolateEdgePosition(new[]
                                    {
                                        new Vector4(w2vectors[0].x, w2vectors[0].y, w2vectors[0].z,
                                            GetCellCornerValue(cellCorner / zoom, (int)w2vectors[0].w)),
                                        new Vector4(w2vectors[1].x, w2vectors[1].y, w2vectors[1].z,
                                            GetCellCornerValue(cellCorner / zoom, (int)w2vectors[1].w))
                                    });

                                    Vector3 interVert3 = InterpolateEdgePosition(new[]
                                    {
                                        new Vector4(w3vectors[0].x, w3vectors[0].y, w3vectors[0].z,
                                            GetCellCornerValue(cellCorner / zoom, (int)w3vectors[0].w)),
                                        new Vector4(w3vectors[1].x, w3vectors[1].y, w3vectors[1].z,
                                            GetCellCornerValue(cellCorner / zoom, (int)w3vectors[1].w))
                                    });

                                    meshVertices.Add(interVert1);
                                    meshVertices.Add(interVert2);
                                    meshVertices.Add(interVert3);
                                }
                                catch (Exception e)
                                {
                                    print(meshVertices.Count / 3);
                                    Console.WriteLine(e);
                                    throw;
                                }
                            }
                        }
                    }
                }
            }
        }

        private float GetCellCornerValue(Vector3Int _localCellCornerCoord, int _cornerId)
        {
            float val = _cornerId switch
            {
                0 => chunkVals[_localCellCornerCoord.x, _localCellCornerCoord.y, _localCellCornerCoord.z + 1],
                1 => chunkVals[_localCellCornerCoord.x + 1, _localCellCornerCoord.y, _localCellCornerCoord.z + 1],
                2 => chunkVals[_localCellCornerCoord.x + 1, _localCellCornerCoord.y, _localCellCornerCoord.z],
                3 => chunkVals[_localCellCornerCoord.x, _localCellCornerCoord.y, _localCellCornerCoord.z],
                4 => chunkVals[_localCellCornerCoord.x, _localCellCornerCoord.y + 1, _localCellCornerCoord.z + 1],
                5 => chunkVals[_localCellCornerCoord.x + 1, _localCellCornerCoord.y + 1, _localCellCornerCoord.z + 1],
                6 => chunkVals[_localCellCornerCoord.x + 1, _localCellCornerCoord.y + 1, _localCellCornerCoord.z],
                7 => chunkVals[_localCellCornerCoord.x, _localCellCornerCoord.y + 1, _localCellCornerCoord.z],
                _ => -1
            };

            return val;
        }

        private Vector3 InterpolateEdgePosition(IList<Vector4> _cornerVectors)
        {
            Vector3 pointOnEdge = Vector3.zero;

            if (Mathf.Approximately(activeVal - _cornerVectors[0].w, 0))
            {
                return new Vector3(_cornerVectors[0].x, _cornerVectors[0].y, _cornerVectors[0].z);
            }

            if (Mathf.Approximately(activeVal - _cornerVectors[1].w, 0))
            {
                return new Vector3(_cornerVectors[1].x, _cornerVectors[1].y, _cornerVectors[1].z);
            }

            if (Mathf.Approximately(_cornerVectors[0].w - _cornerVectors[1].w, 0))
            {
                return new Vector3(_cornerVectors[0].x, _cornerVectors[0].y, _cornerVectors[0].z);
            }

            float mu = (activeVal - _cornerVectors[0].w) / (_cornerVectors[1].w - _cornerVectors[0].w);
            pointOnEdge.x = _cornerVectors[0].x + mu * (_cornerVectors[1].x - _cornerVectors[0].x);
            pointOnEdge.y = _cornerVectors[0].y + mu * (_cornerVectors[1].y - _cornerVectors[0].y);
            pointOnEdge.z = _cornerVectors[0].z + mu * (_cornerVectors[1].z - _cornerVectors[0].z);

            return pointOnEdge;
        }

        private Vector4[] GetCellCornersCoords(Vector3 _cellCorner, int _cellWallIndex)
        {
            Vector3 _right = Vector3.right * zoom;
            Vector3 _forward = Vector3.forward * zoom;
            Vector3 _up = Vector3.up * zoom;

            Vector3 _corner0 = _cellCorner + _forward;
            Vector3 _corner1 = _cellCorner + _forward + _right;
            Vector3 _corner2 = _cellCorner + _right;
            Vector3 _corner4 = _cellCorner + _up + _forward;
            Vector3 _corner3 = _cellCorner;
            Vector3 _corner5 = _cellCorner + _up + _forward + _right;
            Vector3 _corner6 = _cellCorner + _up + _right;
            Vector3 _corner7 = _cellCorner + _up;

            Vector4 corner0 = new(_corner0.x, _corner0.y, _corner0.z, 0);
            Vector4 corner1 = new(_corner1.x, _corner1.y, _corner1.z, 1);
            Vector4 corner2 = new(_corner2.x, _corner2.y, _corner2.z, 2);
            Vector4 corner3 = new(_corner3.x, _corner3.y, _corner3.z, 3);
            Vector4 corner4 = new(_corner4.x, _corner4.y, _corner4.z, 4);
            Vector4 corner5 = new(_corner5.x, _corner5.y, _corner5.z, 5);
            Vector4 corner6 = new(_corner6.x, _corner6.y, _corner6.z, 6);
            Vector4 corner7 = new(_corner7.x, _corner7.y, _corner7.z, 7);


            return _cellWallIndex switch
            {
                0 => new[] { corner0, corner1 },
                1 => new[] { corner1, corner2 },
                2 => new[] { corner2, corner3 },
                3 => new[] { corner0, corner3 },
                4 => new[] { corner4, corner5 },
                5 => new[] { corner5, corner6 },
                6 => new[] { corner6, corner7 },
                7 => new[] { corner4, corner7 },
                8 => new[] { corner0, corner4 },
                9 => new[] { corner1, corner5 },
                10 => new[] { corner2, corner6 },
                11 => new[] { corner3, corner7 },
                _ => new[] { Vector4.zero, Vector4.zero }
            };
        }

        private Vector3 GetTriangleWorldCoord(Vector3 _cellCorner, int _cellWallIndex)
        {
            int wallMod = zoom / 2;

            Vector3 _left = Vector3.left * wallMod;
            Vector3 _right = Vector3.right * wallMod;
            Vector3 _forward = Vector3.forward * wallMod;
            Vector3 _back = Vector3.back * wallMod;
            Vector3 _up = Vector3.up * wallMod;
            Vector3 _down = Vector3.down * wallMod;

            Vector3 cellCenter = _cellCorner + _up + _forward + _right;

            return _cellWallIndex switch
            {
                0 => cellCenter + _down + _forward,
                1 => cellCenter + _down + _right,
                2 => cellCenter + _down + _back,
                3 => cellCenter + _down + _left,
                4 => cellCenter + _up + _forward,
                5 => cellCenter + _up + _right,
                6 => cellCenter + _up + _back,
                7 => cellCenter + _up + _left,
                8 => cellCenter + _forward + _left,
                9 => cellCenter + _forward + _right,
                10 => cellCenter + _back + _right,
                11 => cellCenter + _back + _left,
                _ => cellCenter
            };
        }

        [SuppressMessage("ReSharper", "CompareOfFloatsByEqualityOperator")]
        private void BuildMesh()
        {
            mesh.Clear();
            List<int> meshTris = new();

            for (int i = 0; i < meshVertices.Count / 3; i++)
            {
                for (int j = 0; j < 3; j++)
                {
                    meshTris.Add(i * 3 + j);
                }
            }

            mesh.vertices = meshVertices.ToArray();
            mesh.triangles = meshTris.ToArray();
            mesh.RecalculateNormals();
        }

        // public void EditTerrain(Vector3 coords, float miningLaserStr, int miningLaserRange)
        // {
        //     Vector3Int chunkValCoord = new(Mathf.RoundToInt(coords.x / zoom), Mathf.RoundToInt(coords.y / zoom),
        //         Mathf.RoundToInt(coords.z / zoom));
        //     
        //     foreach (Chunk chunk in ChangeValues(chunkValCoord, miningLaserStr, miningLaserRange))
        //     {
        //         chunk.UpdateMesh();
        //     }
        //
        //     UpdateMesh();
        //
        //     return;
        //         // $"Coord: {new Vector3Int(chunkValCoord.x, chunkValCoord.y, chunkValCoord.z)}\nValue:  {chunkVals[chunkValCoord.x, chunkValCoord.y, chunkValCoord.z]}";
        // }

        // ReSharper disable Unity.PerformanceAnalysis
        // private List<Chunk> ChangeValues(Vector3Int tgtCoord, float laserStrength, int radius)
        // {
        //     List<Chunk> neighborChunksAffected = new();
        //     
        //     // do a half of a change here because the center (target hit) is affected a second time below, leading to a "full" change
        //     chunkVals[tgtCoord.x, tgtCoord.y, tgtCoord.z] += laserStrength / 2;
        //
        //     for (int x = -radius; x < radius + 1; x++)
        //     {
        //         for (int y = -radius; y < radius + 1; y++)
        //         {
        //             for (int z = -radius; z < radius + 1; z++)
        //             {
        //                 try
        //                 {
        //                     if (Mathf.Min(Mathf.Min(tgtCoord.x + x, tgtCoord.y + y), tgtCoord.z + z) > 0 && 
        //                         Mathf.Max(Mathf.Max(tgtCoord.x + x, tgtCoord.y + y), tgtCoord.z + z) <= chunkSize)
        //                     {
        //                         // change is fully enclosed in this chunk
        //                         chunkVals[tgtCoord.x + x, tgtCoord.y + y, tgtCoord.z + z] += laserStrength / 2;
        //
        //                         chunkVals[tgtCoord.x + x, tgtCoord.y + y, tgtCoord.z + z] =
        //                             Mathf.Clamp(chunkVals[tgtCoord.x + x, tgtCoord.y + y, tgtCoord.z + z], 0f, 1f);
        //                     }
        //                     else
        //                     { // changes bleed over into a neighbor chunk
        //                         Chunk tgtChunk;
        //                         // Vector3Int coordMod = new(chunkSize, chunkSize, chunkSize);
        //                         
        //                         if (tgtCoord.x + x > chunkSize)
        //                         {
        //                             tgtChunk = transform.parent.Find($"Chunk-{chunkWorldCoords.x + 1},{chunkWorldCoords.y},{chunkWorldCoords.z}").GetComponent<Chunk>();
        //                             tgtChunk.MineNeighbor(new Vector3Int(x,y,z), laserStrength);
        //                             
        //                             if (!neighborChunksAffected.Contains(tgtChunk)) neighborChunksAffected.Add(tgtChunk);
        //                         }
        //                     }
        //                 }
        //                 catch (Exception e)
        //                 {
        //                     print($"Error: " + new Vector3Int(tgtCoord.x + x, tgtCoord.y + y, tgtCoord.z + z));
        //                     Console.WriteLine(e);
        //                 }
        //             }
        //         }
        //     }
        //
        //     return neighborChunksAffected;
        // }

        // public void MineNeighbor(Vector3Int singleCoord, float laserStr)
        // {
        //     if (Mathf.Min(Mathf.Min(singleCoord.x, singleCoord.y), singleCoord.z) > 0 && 
        //         Mathf.Max(Mathf.Max(singleCoord.x, singleCoord.y), singleCoord.z) < chunkSize)
        //     {
        //         chunkVals[singleCoord.x, singleCoord.y, singleCoord.z] += laserStr / 2;
        //     }
        // }


        public void ChangeValue(Vector3Int localCoord, float changeAmt)
        {
            chunkVals[localCoord.x, localCoord.y, localCoord.z] += changeAmt;
        }
        
        private void OnDrawGizmos()
        {
            Gizmos.color = activeCol;

            if (drawChunkBorders) // border
            {
                Gizmos.DrawWireCube(
                    new Vector3(
                        chunkCorner.x + (chunkSize * zoom / 2f),
                        chunkCorner.y + (chunkSize * zoom / 2f),
                        chunkCorner.z + (chunkSize * zoom / 2f)),
                    new Vector3(
                        (chunkSize * zoom),
                        (chunkSize * zoom),
                        (chunkSize * zoom)));
            }

            if (drawSpheres) // spheres
            {
                if (chunkVals == null)
                {
                    for (int i = 0; i < actualChunkSize; i++)
                    {
                        for (int j = 0; j < actualChunkSize; j++)
                        {
                            for (int k = 0; k < actualChunkSize; k++)
                            {
                                Vector3 spherePos = new(
                                    (i * zoom) + chunkCorner.x,
                                    (j * zoom) + chunkCorner.y,
                                    (k * zoom) + chunkCorner.z
                                );

                                Gizmos.color = activeCol;
                                Gizmos.DrawSphere(spherePos, sphereRadius);
                            }
                        }
                    }
                }
                else
                {
                    for (int i = 0; i < actualChunkSize; i++)
                    {
                        for (int j = 0; j < actualChunkSize; j++)
                        {
                            for (int k = 0; k < actualChunkSize; k++)
                            {
                                Vector3 spherePos = new(
                                    (i * zoom) + chunkCorner.x,
                                    (j * zoom) + chunkCorner.y,
                                    (k * zoom) + chunkCorner.z
                                );

                                Gizmos.color = chunkVals[i, j, k] > activeVal ? activeCol : inactiveCol;
                                Gizmos.DrawSphere(spherePos, sphereRadius);
                            }
                        }
                    }
                }
            }

            if (drawLabels) // Handles/Labels
            {
                Handles.Label(GetTriangleWorldCoord(chunkCorner, -1), "Center");
                for (int i = 0; i < 8; i++)
                {
                    Handles.Label(GetTriangleWorldCoord(chunkCorner, i), i.ToString());
                }

                Handles.Label(chunkCorner + Vector3.forward * zoom + Vector3.up * zoom / 2, "8");
                Handles.Label(chunkCorner + Vector3.forward * zoom + Vector3.up * zoom / 2 + Vector3.right * zoom, "9");
                Handles.Label(chunkCorner + Vector3.up * zoom / 2 + Vector3.right * zoom, "10");
                Handles.Label(chunkCorner + Vector3.up * zoom / 2, "11");


                GUI.color = Color.green;
                Handles.Label(chunkCorner + Vector3.forward * zoom, "_0");
                Handles.Label(chunkCorner + Vector3.forward * zoom + Vector3.right * zoom, "_1");
                Handles.Label(chunkCorner + Vector3.right * zoom, "_2");
                Handles.Label(chunkCorner, "_3");
                Handles.Label(chunkCorner + Vector3.forward * zoom + Vector3.up * zoom, "_4");
                Handles.Label(chunkCorner + Vector3.forward * zoom + Vector3.right * zoom + Vector3.up * zoom, "_5");
                Handles.Label(chunkCorner + Vector3.up * zoom + Vector3.right * zoom, "_6");
                Handles.Label(chunkCorner + Vector3.up * zoom, "_7");
            }
        }
    }
}
