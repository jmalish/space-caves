using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace SpaceCaves
{
    public class WorldManager : MonoBehaviour
    {
        private readonly List<Chunk> chunkList = new ();
        
        public Vector3Int worldSize;
        public Material worldMaterial;
        public Material debugMat;
        public PlayerController player;

        [Header("Chunk Settings")]
        public int chunkSize;
        public int zoom;
        [Range(0,100)] public float frequency;
        public int seed;
        [Range(0,1)]public float activeVal;
        public bool useInterpolation;

        [Header("Render Settings")]
        public bool isFogEnabled;
        public int drawDistance = 100;

        [Header("Debug")] 
        public Vector3 playerPosOnStart;


        private void Start()
        {
            StartCoroutine(GenerateChunks(OnGenerateChunkComplete));
        }

        private void Update()
        {
            foreach (Chunk chunk in chunkList)
            {
                chunk.SetActiveStatus(Vector3.Distance(chunk.chunkCenter, player.transform.position) < drawDistance * chunkSize * zoom);
                
                chunk.interpolate = useInterpolation;
            }
        }

        private void OnGenerateChunkComplete()
        {
            PlayerSetup();
            if (!isFogEnabled) return;
            EnableFog();
        }
        
        private void EnableFog()
        {
            RenderSettings.fogMode = FogMode.Linear;
            
            int fogDistance = drawDistance * chunkSize * zoom;
            RenderSettings.fogStartDistance = fogDistance / 2f;
            RenderSettings.fogEndDistance = fogDistance;
        }

        private void PlayerSetup()
        {
            player.worldZoom = zoom;
            // player.transform.position = chunkList.Find(chunk =>
            //     chunk.name == $"Chunk-{(int)(worldSize.x / 2f)},{(int)(worldSize.y / 2f)},{(int)(worldSize.z / 2f)}").chunkCenter;
            
            player.transform.position = playerPosOnStart;
        }

        private IEnumerator GenerateChunks(Action callback)
        {
            int chunksGenerated = 0;
            int totalChunks = (worldSize.x * worldSize.y * worldSize.z);
            print($"Generating {totalChunks} chunks...");
            
            for (int x = 0; x < worldSize.x; x++)
            {
                for (int y = 0; y < worldSize.y; y++)
                {
                    for (int z = 0; z < worldSize.z; z++)
                    {
                        GameObject newChild = new()
                        {
                            transform =
                            {
                                parent = transform,
                                position = new Vector3(
                                    x * zoom * chunkSize,
                                    y * zoom * chunkSize,
                                    z * zoom * chunkSize)
                            }
                        };

                        Chunk newChunk = newChild.AddComponent<Chunk>();

                        Material matUse = x == 0 && y == 0 && z == 0 ? debugMat : worldMaterial;
                        
                        newChunk.Initialize(new Vector3Int(x,y,z), chunkSize, zoom, frequency, seed, activeVal, matUse, useInterpolation);
                        
                        newChunk.SetActiveStatus(false);

                        chunkList.Add(newChunk);

                        chunksGenerated++;
                        print($"Generating... {(float)chunksGenerated / totalChunks * 100f}%");
                        yield return null;
                    }
                }
            }
            
            callback();
        }


        // ReSharper disable Unity.PerformanceAnalysis
        public void HandleMining(RaycastHit hit, float laserStr, int mineRadius)
        {
            List<Chunk> chunksToUpdate = new();

            Vector3 localizedHit = hit.collider.gameObject.transform.InverseTransformPoint(hit.point);
            Vector3Int chunkValCoord = new(Mathf.RoundToInt(localizedHit.x / zoom), Mathf.RoundToInt(localizedHit.y / zoom),
                Mathf.RoundToInt(localizedHit.z / zoom));
            
            Chunk hitChunk = hit.collider.gameObject.GetComponent<Chunk>();
            Vector3Int hitChunkWorldCoords = hitChunk.chunkWorldCoords;

            
            for (int x = -mineRadius; x < mineRadius + 1; x++)
            {
                for (int y = -mineRadius; y < mineRadius + 1; y++)
                {
                    for (int z = -mineRadius; z < mineRadius + 1; z++)
                    {
                        try
                        {
                            Vector3Int editCoord = new(chunkValCoord.x + x, chunkValCoord.y + y, chunkValCoord.z + z);
                            Vector3Int editChunkWorldCoord = hitChunkWorldCoords;

                            if (editCoord.x < 0)
                            {
                                editChunkWorldCoord.x -= 1;
                                editCoord.x += chunkSize;
                            } else if (editCoord.x > chunkSize)
                            {
                                editChunkWorldCoord.x += 1;
                                editCoord.x -= chunkSize;
                            } else if (editCoord.x == 0)
                            {
                                hitChunk.ChangeValue(editCoord, laserStr / 2);
                                if (!chunksToUpdate.Contains(hitChunk)) chunksToUpdate.Add(hitChunk);
                                
                                editChunkWorldCoord.x -= 1;
                                editCoord.x = chunkSize;
                            } else if (editCoord.x == chunkSize)
                            {
                                hitChunk.ChangeValue(editCoord, laserStr / 2);
                                if (!chunksToUpdate.Contains(hitChunk)) chunksToUpdate.Add(hitChunk);
                                
                                editChunkWorldCoord.x += 1;
                                editCoord.x = 0;
                            } else
                            {
                                hitChunk.ChangeValue(editCoord, laserStr / 2);
                                if (!chunksToUpdate.Contains(hitChunk)) chunksToUpdate.Add(hitChunk);
                            }

                            if (editCoord.y < 0)
                            {
                                editChunkWorldCoord.y -= 1;
                                editCoord.y += chunkSize;
                            } else if (editCoord.y > chunkSize)
                            {
                                editChunkWorldCoord.y += 1;
                                editCoord.y -= chunkSize;
                            } else if (editCoord.y == 0)
                            {
                                hitChunk.ChangeValue(editCoord, laserStr / 2);
                                if (!chunksToUpdate.Contains(hitChunk)) chunksToUpdate.Add(hitChunk);
                                
                                editChunkWorldCoord.y -= 1;
                                editCoord.y = chunkSize;
                            } else if (editCoord.y == chunkSize)
                            {
                                hitChunk.ChangeValue(editCoord, laserStr / 2);
                                if (!chunksToUpdate.Contains(hitChunk)) chunksToUpdate.Add(hitChunk);
                                
                                editChunkWorldCoord.y += 1;
                                editCoord.y = 0;
                            } else
                            {
                                hitChunk.ChangeValue(editCoord, laserStr / 2);
                                if (!chunksToUpdate.Contains(hitChunk)) chunksToUpdate.Add(hitChunk);
                            }
                            
                            if (editCoord.z < 0)
                            {
                                editChunkWorldCoord.z -= 1;
                                editCoord.z += chunkSize;
                            } else if (editCoord.z > chunkSize)
                            {
                                editChunkWorldCoord.z += 1;
                                editCoord.z -= chunkSize;
                            } else if (editCoord.z == 0)
                            {
                                hitChunk.ChangeValue(editCoord, laserStr / 2);
                                if (!chunksToUpdate.Contains(hitChunk)) chunksToUpdate.Add(hitChunk);
                                
                                editChunkWorldCoord.z -= 1;
                                editCoord.z = chunkSize;
                            } else if (editCoord.z == chunkSize)
                            {
                                hitChunk.ChangeValue(editCoord, laserStr / 2);
                                if (!chunksToUpdate.Contains(hitChunk)) chunksToUpdate.Add(hitChunk);
                                
                                editChunkWorldCoord.z += 1;
                                editCoord.z = 0;
                            } else
                            {
                                hitChunk.ChangeValue(editCoord, laserStr / 2);
                                if (!chunksToUpdate.Contains(hitChunk)) chunksToUpdate.Add(hitChunk);
                            }

                            
                            if (editChunkWorldCoord != hitChunkWorldCoords)
                            {
                                string chunkNeighborName =
                                    $"Chunk-{editChunkWorldCoord.x},{editChunkWorldCoord.y},{editChunkWorldCoord.z}";

                                Chunk neighborChunk = GameObject.Find(chunkNeighborName).GetComponent<Chunk>();
                                
                                neighborChunk.ChangeValue(editCoord, laserStr / 2);
                                if (!chunksToUpdate.Contains(neighborChunk)) chunksToUpdate.Add(neighborChunk);
                            }
                            else
                            {
                                hitChunk.ChangeValue(editCoord, laserStr / 2);
                                if (!chunksToUpdate.Contains(hitChunk)) chunksToUpdate.Add(hitChunk);
                            }
                        }
                        catch (Exception e)
                        {
                            Console.WriteLine(e);
                            print("Error: " + new Vector3Int(chunkValCoord.x + x, chunkValCoord.y + y,
                                chunkValCoord.z + z));
                        }
                    }
                }
            }
            
            foreach (Chunk chunk in chunksToUpdate)
            {
                chunk.UpdateMesh();
            }
        }
    }
}
