using System.Collections;
using UnityEngine;

namespace SpaceCaves
{
    public class PlayerController : MonoBehaviour
    {
        public WorldManager worldManager;
        public float moveSpeed = 5f;
        public float rotationSpeed = 180f;
        public float lookSensitivity = 2f;

        private Transform playerTrans;
        private Vector3 playerPos;
        private CharacterController controller;
        private Transform cameraTransform;
        private float pitch;
        private float yaw;

        [Header("Mining")]
        private LineRenderer laserRenderer;
        public float laserLength = 50;
        public float mineCoolDown = .1f;
        private bool isMiningAllowed = true;
        public float miningLaserStr = .1f;
        public int miningLaserRange = 1;

        [Header("UI/Debug Text")] 
        [HideInInspector]public int worldZoom;
        public DebugUI targeting;


        private void Start()
        {
            playerTrans = transform;
            controller = GetComponent<CharacterController>();
            if (Camera.main != null) cameraTransform = Camera.main.transform;
            Cursor.lockState = CursorLockMode.Locked;
            Cursor.visible = false;

            laserRenderer = GetComponent<LineRenderer>();
        }
        

        private void Update()
        {
            playerPos = transform.position;
            HandleMovement();

            HandleLaserFiring();
            
            Vector3 cameraForward = cameraTransform.forward;
            Ray ray = new(playerPos, cameraForward);
            
            if (Physics.Raycast(ray, out RaycastHit hit, laserLength))
            {
                Vector3 coords = hit.collider.gameObject.transform.InverseTransformPoint(hit.point) / worldZoom;
                Vector3Int chunkValCoord = new (Mathf.RoundToInt(coords.x), Mathf.RoundToInt(coords.y),
                    Mathf.RoundToInt(coords.z));
                targeting.UpdateText($"Targeting: {hit.collider.gameObject.name}\t{coords} = {chunkValCoord}");
            }
        }

        private void HandleLaserFiring()
        {
            if (!isMiningAllowed) return;
            
            
            bool fire1 = Input.GetAxis("Fire1") > 0;
            bool fire2 = Input.GetAxis("Fire2") > 0;
            
            EnableLineRenderer(fire1 || fire2);
            
            if (!fire1 && !fire2) return;

            
            Vector3 cameraForward = cameraTransform.forward;
            laserRenderer.SetPosition(0, playerPos + cameraForward * .1f);
            
            Ray ray = new(playerPos, cameraForward);
            if (!Physics.Raycast(ray, out RaycastHit hit, laserLength)) return;
            
            
            laserRenderer.SetPosition(1, hit.point);
            GameObject hitObject = hit.collider.gameObject;

            if (hitObject.CompareTag("Terrain"))
            {
                worldManager.HandleMining(hit, fire2 ? miningLaserStr : -miningLaserStr, miningLaserRange);
                StartCoroutine(MiningCoolDown());
            }
            else
            {
                print("Not terrain - " + hit.collider.gameObject.name);
            }
        }

        private void EnableLineRenderer(bool active)
        {
            laserRenderer.enabled = active;
        }

        private void HandleMovement()
        {
            float verticalInput = Input.GetAxis("Vertical");
            float horizInput = Input.GetAxis("Horizontal");
            float ascentInput = Input.GetAxis("Ascend");
            
            float mouseX = Input.GetAxis("Mouse X");
            float mouseY = Input.GetAxis("Mouse Y");

            // Calculate movement vector
            Vector3 forMovement = playerTrans.forward * (verticalInput * moveSpeed * Time.deltaTime);
            Vector3 latMovement = playerTrans.right * (horizInput * moveSpeed * Time.deltaTime);
            Vector3 ascMovement = playerTrans.up * (ascentInput * moveSpeed * Time.deltaTime);

            // Calculate rotation
            yaw += mouseX * rotationSpeed * Time.deltaTime;
            pitch -= mouseY * lookSensitivity;
            pitch = Mathf.Clamp(pitch, -90f, 90f);

            // Apply rotation to player and camera
            playerTrans.rotation = Quaternion.Euler(pitch, yaw, 0f);
            cameraTransform.localRotation = Quaternion.Euler(0f, 0f, 0f);

            // Move the character controller
            controller.Move(forMovement + latMovement + ascMovement);
        }
        
        private IEnumerator MiningCoolDown()
        {
            isMiningAllowed = false;
            yield return new WaitForSeconds(mineCoolDown);
            isMiningAllowed = true;
        }
    }
}
