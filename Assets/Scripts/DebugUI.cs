using System;
using TMPro;
using UnityEngine;

namespace SpaceCaves
{
    public class DebugUI : MonoBehaviour
    {
        private TextMeshProUGUI tmpText;
        
        private void Start()
        {
            tmpText = GetComponent<TextMeshProUGUI>();
        }

        public void UpdateText(string text)
        {
            if (text != null)
            {
                tmpText.SetText(text);
            }
        }
    }
}
